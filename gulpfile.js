var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
const prefix    = require('gulp-autoprefixer');
const plumber   = require('gulp-plumber');
const pug       = require('gulp-pug');
const wait      = require('gulp-wait');
const reload    = browserSync.reload;

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch("./scss/**/*.scss", ['css']);
    gulp.watch("./*.html").on('change', reload);
    gulp.watch('./js/**/*.js', reload)
});

gulp.task('css', function(){
    return gulp.src('./scss/main.scss')
    .pipe(sass())
    .pipe(prefix())
    .pipe(gulp.dest('./css'))
    .pipe(wait(200))
    .pipe(browserSync.stream())
});

gulp.task('html', () => {
    return gulp.src('./views/*.pug')
    .pipe(pug())
    .pipe(gulp.dest('./'))
    .on('end', reload)
});

gulp.task('default', ['browser-sync', 'html', 'css'])