$(".installment-area-card-is-selected").on('click', function(event) {
    var radio_selector = 'input[type="radio"]',
        $radio;

    $('.installment-area-card-is-selected').removeClass('installment-selected');

    if (!$(event.target).is(radio_selector)) {
        $radio = $(this).find(radio_selector);
        event.stopImmediatePropagation();
        $radio.prop('checked', !$radio.is(':checked'));
        $(this).addClass('installment-selected');
        event.preventDefault();
    }
    return false;
});

