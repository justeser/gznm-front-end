//Mobilde sağ menüyü açıp kapatıyor...
$('#openRightMenu').on('click', function () {

    if($('#rightColumn').is('.show-right-column')) {
        $('.container').css({"height": "auto", "overflow": "visible"});
    } else {
        var rightColumnHeight = $('.right-column').height();
        $('.container').css({"height": rightColumnHeight + 70, "overflow": "hidden"});
    }

    $(this).rotate(180, {
        duration: 200,
        easing: 'swing',
        complete: function() {
            $('.right-column').toggleClass('show-right-column');
            $('.left-column').toggleClass('hide-left-column');
            $(document).scrollTop(0);
        }
    });   

});


