
$(function() {
    $('.isStep2Btn').hide();
    $('.isStep3Btn').hide();
    window.location.hash = '#step1';
    $(window).on('hashchange', function() {
        var hash = location.hash;
        if(hash == '#step1'){
            $('#step1').show();
                      
            $('#step2').hide();
            $('#step3').hide();
            $('.step1-inner').removeClass('step-active').addClass('step-ok');
            $('.step2-inner').removeClass('step-active').removeClass('step-ok');
            $('.step3-inner').removeClass('step-active').removeClass('step-ok');

            $('.isStep1Btn').show();
            $('.isStep2Btn').hide(); 
            $('.isStep3Btn').hide(); 
        }else if (hash == '#step2') {
            $('#step2').show();
            $('.step1-inner').removeClass('step-ok').addClass('step-active');
            $('.step2-inner').removeClass('step-active').addClass('step-ok');
            $('.step3-inner').removeClass('step-active').removeClass('step-ok');
            $('#step1').hide();
            $('#step3').hide();

            $('.isStep2Btn').show();   
            $('.isStep1Btn').hide();   

        }else if (hash == '#step3') {
            $('#step3').show();
            $('.step3-inner').addClass('step-ok');
            $('.step2-inner').removeClass('step-ok');
            $('.step2-inner').addClass('step-active');
            $('.step3-inner').removeClass('step-active');
            $('#step1').hide();
            $('#step2').hide();

            $('.isStep1Btn').hide();   
            $('.isStep2Btn').hide(); 
            $('.isStep3Btn').show(); 
        }else{
            $('#step1').show();
                      
            $('#step2').hide();
            $('#step3').hide();
            $('.step1-inner').removeClass('step-active').addClass('step-ok');
            $('.step2-inner').removeClass('step-active').removeClass('step-ok');
            $('.step3-inner').removeClass('step-active').removeClass('step-ok');

            $('.isStep1Btn').show();
            $('.isStep2Btn').hide(); 
            $('.isStep3Btn').hide();             
        }
        e.preventDefault();
    });
});
