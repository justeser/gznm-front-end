// 1. sayfa 
$('.filter-departure-rank, .filter-arrival-rank, .filter-price-rank').on('click', function () {
    $(this).children('i').rotate(180, {
        duration: 500,
        easing: 'swing',
        complete: function() {
        }
    });    
});


// uçuş filtreleme alanının gösterimi
$('#departure-filter-btn').on('click', function () {
    $('#departure-filter-area').slideToggle('fast');
});

$('#arrival-filter-btn').on('click', function () {
    $('#arrival-filter-area').slideToggle('fast');
});
//----

//-- ulaşım istemiyorum seçilmiş ise 

//default olarak ulaşım istemiyorum seçili
$('.notTransportation').hide();

$( ".isTransportation" ).on( "click", function() {
    var data = $(this).attr('data-transportation');
    if(data == 'notTransportation'){
        $('.notTransportation').hide();
    }else if (data == 'airPlane'){
        $('.notTransportation').show();
        $('.onlyExport').hide();
        directionControl();
    }else if (data == 'Export') {
        $('.notTransportation').hide();
        $('.onlyExport').show();
    }else if (data == 'airplaneAndExport') {
        $('.notTransportation').show(); 
        $('.onlyExport').show();
        directionControl();
    }
});


$( ".isDirection" ).on( "click", function() {
    directionControl();
});

function directionControl () {
    var isDirection = $("#isSelectDirection input[type='radio']:checked").val();
    if(isDirection == 'oneDirection') {
        $('.oneDirection').show();
        $('.twoDirection').hide();
    }else{
        $('.oneDirection').show();
        $('.twoDirection').show();
    }
}

// $('.flights-cancel').on('click', function() {
//     $('#transportation').prop('checked', 'checked');
// });

// Ek hizmetler, gidiş-Dönüş uçuşları input['radio] seçimleri
$(".otel-additional-services").on('click', function(event) {
    var radio_selector = 'input[type="radio"]',
        $radio;
        
    if (!$(event.target).is(radio_selector)) {
        $radio = $(this).find(radio_selector);
        $radio.prop('checked', !$radio.is(':checked'));
        $(this).toggleClass('otel-additional-services-active');
        event.stopImmediatePropagation();
        event.preventDefault();
    }
    return false;
});

$(".airline-transfer-select-box").on('click', function(event) {
    var radio_selector = 'input[type="radio"]',
        $radio;
        
    if (!$(event.target).is(radio_selector)) {
        $radio = $(this).find(radio_selector);
        $radio.prop('checked', !$radio.is(':checked'));
        $(this).toggleClass('airline-transfer-select-box-active');
        event.stopImmediatePropagation();
        event.preventDefault();
    }
    return false;
});

$(".departure-flight-list").on('click', function(event) {
    var radio_selector = 'input[type="radio"]',
        $radio;
        
    $('.departure-flight-list').removeClass('departure-flight-list-active');
    $('.departure-flight-list').children().children('.option-input').prop('checked', "");

    if (!$(event.target).is(radio_selector)) {
        $radio = $(this).find(radio_selector);
        $radio.prop('checked', !$radio.is(':checked'));
        $(this).toggleClass('departure-flight-list-active');
        event.stopImmediatePropagation();
        event.preventDefault();
    }

    var data = $(this).attr('data-flight-no');
    data = data.slice(0,2);

	var filterItems = $('[data-filter-item]');

	if ( data != '' ) {
		filterItems.addClass('hidden');
		$('[data-filter-item][data-flight-no*="' + data + '"]').removeClass('hidden');
	} else {
        alert('test 2');
		filterItems.removeClass('hidden');
	}

    return false;
});

$(".return-flight-list").on('click', function(event) {
    var radio_selector = 'input[type="radio"]',
        $radio;
        
    $('.return-flight-list').removeClass('return-flight-list-active');
    $('.return-flight-list').children().children('.option-input').prop('checked', "");

    if (!$(event.target).is(radio_selector)) {
        $radio = $(this).find(radio_selector);
        $radio.prop('checked', !$radio.is(':checked'));
        $(this).toggleClass('return-flight-list-active');
        event.stopImmediatePropagation();
        event.preventDefault();
    }
    return false;
});
//-----

//Uçuş sorgulama
$('#getFlightList').on('click', function(){
    $('.isFlights').slideToggle();
});

$('.isFlights').hide();
//uçuş kişi sayısı
$(".flight-person-num").focus(function() {  
    $(this).siblings('.dropDownFlightPersonSelect').fadeIn(500);
});

$('*').click(function(e){
    if ( !$(e.target).is('.flightPersonSelectArea') && !$(e.target).is('.flightPersonSelectArea *') ){
        $('.dropDownFlightPersonSelect').fadeOut(300);
    }
});


// yolcu sayısı seçim
$(".plus").click(function(e) {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.siblings('input');
    var value = parseInt($input.val());

    if (value < 9) {
    value = value + 1;
    } 
    else {
    value = 9;
    }
    $input.val(value);
    passengerChange();
});

$(".minus").click(function(e) {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.siblings('input');
    var value = parseInt($input.val());

    if (value > 1) {
    value = value - 1;
    } 
    else {
    value =1;
    }
    $input.val(value);
    passengerChange();
});

function passengerChange() {
    var adults = $('input[name=numberOfAdults]').val();
    var childs = $('input[name=numberOfChilds]').val();
    var babies = $('input[name=numberOfBabies]').val();
    var isChilds = childs > 0 ? ' ,' + childs + ' Çocuk' : '';
    var isBabies = babies > 0 ? ' ,' + babies + ' Bebek' : '';
    var numbers = adults +' Yetişkin' + isChilds + isBabies;
    $('input[name=numberOfPassenger]').val(numbers);
}
passengerChange();
