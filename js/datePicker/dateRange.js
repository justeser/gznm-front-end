$(document).ready(function(){
    var resmiBayramlar = {
      '2019-01-01' : 'Yılbaşı',
      '2019-04-23' : 'Ulusal Egemenlik ve Çocuk Bayramı',
      '2019-05-19' : 'Spor ve Gençlik Bayramı',
      '2019-08-30' : 'Zafer Bayramı'
    };
    $('.t-datepicker').tDatePicker({
      fnDataEvent: resmiBayramlar,
    });

    $('.t-datepicker .single-date').tDatePicker({
      dateCheckIn: new Date(),
      dateCheckOut: new Date(),
      autoClose: true,
      limitNextMonth: 3,
      numCalendar : 1,
      dateRangesHover: false
    });
});
