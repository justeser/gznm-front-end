$(function() {
    $(".room-remove").click(function(){
        $('.addRoomWarn').html('');
        $(this).parent('.room-person-select').parent().hide();
        $(this).parent('.room-person-select').parent().removeClass('roomNumber');
        $(this).siblings('.newChild').remove();
    });

    $("#addRoom").click(function(){
        var roomLenght = $('#rooms').children('.roomNumber').length;
        if(roomLenght == 1){
            $('#twoRoom').show();
            $('#twoRoom').addClass('roomNumber');
        } else if (roomLenght == 2) {
            $('#thereRoom').show();
            $('#thereRoom').addClass('roomNumber');
            $('#addRoom').addClass('room-passive');
        } else {
            $('.addRoomWarn').html('(En fazla 3 oda seçebilirsiniz!)')
        }
       // $(".room-person-select").clone().appendTo("#rooms");
    });

    $(".numberOfChild").on('change', function(ret) {  
        var i;
        $(this).parent().parent().parent().children('.newChild').remove();
        for (i = 0; i < ret.target.value; i++) { 
            $(this).parent().parent().parent().append(
                '<div class="room-child newChild">' + 
                    '<div class="form-group">' +
                        '<label for="">'+ (i + 1) +'. Çocuk Yaşı</label>' +
                        '<select name="" class="form-control">' +
                            '<option value="1">1</option>' +
                            '<option value="2">2</option>' +
                            '<option value="4">4 </option>' +
                            '<option value="5">5 </option>' +
                            '<option value="6">6 </option>' +
                            '<option value="7">7 </option>' +
                            '<option value="8">8 </option>' +
                            '<option value="9">9 </option>' +
                            '<option value="10">10 </option>' +
                            '<option value="11">11 </option>' +
                            '<option value="12">12 </option>' +
                        '</select>' +
                    '</div>' +
                '</div>');                   
        }

   });

});