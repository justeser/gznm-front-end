$(function(){
    var tab = $('.tab-menu-button-area div'),
        content = $('.tab-content article');

        tab.filter(':first').addClass('tab-active');
        tab.children('i').addClass('icon-main-search-bell-active');
        content.filter(':not(:first)').hide();

        tab.click(function(){
            var indis = $(this).index();
            tab.removeClass('tab-active').eq(indis).addClass('tab-active');
            if(indis == 0){
                tab.siblings().children('i').removeClass('icon-main-search-plane-active').removeClass('icon-main-search-map-active');
                $(this).children('i').addClass('icon-main-search-bell-active');
            } else if (indis == 1) {
                tab.siblings().children('i').removeClass('icon-main-search-plane-active').removeClass('icon-main-search-bell-active');
                $(this).children('i').addClass('icon-main-search-map-active');
            } else if (indis == 2) {
                tab.siblings().children('i').removeClass('icon-main-search-bell-active').removeClass('icon-main-search-map-active');
                $(this).children('i').addClass('icon-main-search-plane-active');
            }
            content.hide().eq(indis).fadeIn(500);
        });
});