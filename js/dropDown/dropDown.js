$(function() {
    $(".inputFocus").focus(function() {  
        $(this).siblings('.dropDownList').fadeIn(500);
    });
    
    $('*').click(function(e){
        if ( !$(e.target).is('.dropDownArea') && !$(e.target).is('.dropDownArea *') ){
            $('.dropDownList').fadeOut(300);
        }
    });
});